@extends('layout')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  <div class="card-header">
    Add Post
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('posts.store') }}">
          <div class="form-group">
              @csrf
              <label for="name">Main Title:</label>
              <input type="text" class="form-control" name="main_title"/>
          </div>
          <div class="form-group">
              <label for="price">Description :</label>
              <input type="text" class="form-control" name="sub_title"/>
          </div>
          <div class="form-group">
              <label for="quantity">Content:</label>
              <textarea rows="4" cols="50" class="form-control" name="article">
              </textarea>
          </div>
          <div class="form-group">
                <label for="quantity">Author:</label>
                <input type="text" class="form-control" name="author"/>
            </div>
          <button type="submit" class="btn btn-primary float-right">Add</button>
      </form>
  </div>
</div>
@endsection