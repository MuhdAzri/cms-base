@extends('layout')

@section('content')
<style>
    .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
    <div class="card-header">
        Edit Post
    </div>
    <div class="card-body">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br />
        @endif
        <form method="post" action="{{ route('posts.update', $post->id) }}">
            @method('PATCH')
            @csrf
            <div class="form-group">
                <label for="title">Title:</label>
                <input type="text" class="form-control" name="main_title" value="{{ $post->main_title }}" />
            </div>
            <div class="form-group">
                <label for="title">Sub Title:</label>
                <input type="text" class="form-control" name="sub_title" value="{{ $post->sub_title }}" />
            </div>
            <div class="form-group">
                <label for="article">Article:</label>
                <input type="text" class="form-control" name="article" value="{{ $post->article }}" />
            </div>
            <div class="form-group">
                <label for="author">Author:</label>
                <input type="text" class="form-control" name="author" value="{{ $post->author }}" />
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
</div>
@endsection
